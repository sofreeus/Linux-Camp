Linux Camp EXTRA Packing
========================

In addition to [normal packing](https://gitlab.com/sofreeus/sofreeus/blob/master/packing.md)

    Print cheat-sheets
    ISO's for distributions used
    Network switch, wireless bridge, long cables, cable-ties
    Presenter's laptop, server, caching web-proxy
    Spare laptops
    Games and Toys
    - frisbee
    - blocks
    - pyramids
    - zometool
    - buckyballs
    Books, etc...
    Just for Fun, Free as in Freedom, The Cathedral and the Bazaar
    Revolution video
    Various O'Reilly books
    Robe / pajamas
    Recycling bins
    Notepads
    Whiteboard and big stickies
    Peace, Love, and Linux woods
