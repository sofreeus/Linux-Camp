Content of the LFCS
===

### Editing

Be proficient with your editor of choice. Mine is vim. I will talk through my use of keys.

Exercises: Write scripts and (markdown) docs.

### Finding files and content

Be proficient with find and grep.

 * 'find' files by size/name/ownership/perms
 * 'grep -R|--recursive' to find files by content

### Compression and Archiving

Be competent with tar, zip, bzip2, gzip, etc.

Exercises: Backups, restores, installs from source (unpack, configure, make, make install)

### Manage Packages and Package Repositories

rpm/yum, dpkg/apt, rpm/zypper, source installs

Exercises:

What packages do I have?

Which package owns (file)?

Which available package would provide "*bin/httpd"?

Also consider groups (yum), tasks (apt/tasksel), bundles? (zypper)

### Manage Local Users and Groups

 * Create and remove users and groups: change /etc/skel, change shell and/or home-dir
 * Add users to secondary groups.
 * Expire/unexpire/suspend users.
 * Set memory and CPU limits. (nprocs)
 * visudo, /etc/sudoers
 * /etc/skel


### Manage File Permissions

chmod


### Manage Local Storage and File-Systems

 * fdisk/cfdisk/parted
 * lvm - pv*,vg*,lv*
 * mkfs*
 * /etc/fstab
 * mount/umount


### Manage Network File-Systems (client ops)

 * nfs
 * samba (optional)


### Schedule jobs

 * cron
 * at (optional)


### Script

basic bash scripts


### Networking

 * configure static network settings
 * manipulate hosts entries
