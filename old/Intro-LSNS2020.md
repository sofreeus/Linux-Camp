LSNS 2020 using Linux Camp mats 
==========


Introduction
------------

### About the instructor

    David L. Willson
    RHCE(+Satellite), CCAH, LPIC-1, Novell CLP, LFCS, COA
    Former MCT, MCSE
    25+ years in IT, 15+ years teaching Linux
    
    Gary Romero
    LFCS(2015)
    Associate DevOps Engineer at Daxko
    15 years in IT, 8 years teaching Open Source Software

### About the location

The Comfy Room - 4237 W. Grand Ave, Littleton, CO 80123

Zoom - https://zoom.us/j/664404898

Trouble? Call DLW @ 720-333-5267

### Weekly schedule

We'll meet every week for eight weeks on Monday night, from 7PM until at least 9PM. Please give yourself (and us) until 10PM to finish labs and exploration. It's often best to 'strike when the iron is hot'.

Find at least an hour, preferably more, up to 3 hours, to study independently, to reinforce what you've learned and expand on it.

### Format

Please try to work in small groups of 2 or 3. You'll integrate the new tools and knowledge better when you go through the activities 2 or 3 times, at least once as the operator/novice and at least once as an advisor/expert. You'll learn more as you discuss the details of assignments, and argue about the merits of different approaches. Your focus will be better. You'll have more fun. So, for each assigned task, one of you will take the role of operator and the other(s) will take the role of assistants and/or advisors. In any case, only one of you should be working on the keyboard. The other should be actively assisting: reading the instructions aloud, helping with command syntax, tactics, and strategy, in other words, thinking about the tasks.

    SA = System Administrator
    SE = Systems Engineer

You are playing the roles of new SA's (Alice and Bob) at MacGuffins. We will work from “emails” from the somewhat technically savvy company-owner (Jan). Jan will ask you to do certain things, in certain ways. Jan will only be as specific as necessary to communicate the request. For exercises where you're likely to need a little nudge, you'll have hints from the company SE, Kim. Neither Jan nor Kim will feed you a prescriptive solution. They respect you too much for that. They think you know good answers, and can figure out good solutions to new problems as a team, that you'll ask for clarification or guidance when appropriate.

### Distributions

We will use the distro-versions that can be chosen as exam platforms. The Linux Foundation is distribution and vendor neutral, but a practical exam must be specific. Presently, the exam distributions are: CentOS 7 and Ubuntu 18.04.

### Virtualization - VBox and KVM

We're using Oracle VirtualBox because it's software libre and it allows all the functionality we'll need for this class. The default "system key" for VirtualBox is [right-control]. Tap the system key to release the keyboard and mouse, or hold it to simulate [control]+[alt] for [ctrl]+[alt]+[del], [ctrl]+[alt]+[F1], and so on.

You'll need a physical machine to host KVM. If you don't have one, please let us know so we can get you a loaner. We have loaners available, we just need to know you need one.

### Strategy and Tips

Script everything. Monkey-script whatever you can't bash.

Monkey-script? If you at least create a text file listing all the things you clicked and typed as you went along, when you repeat that process, you won't miss important steps. And, if you clean up your list of commands, reducing it to the necessary minimum, you might end up with a good, re-usable "monkey-script", and you'll be one step close to smart-lazy Nirvana.

In the real world, this "script everything" pays off every time you get asked how or whether you did something, and when you do such a good job on something that you get asked to do it again.

You might find this [CheatSheet](CheatSheet.pdf) handy as you're doing labs, or in real life.

### Does Linux matter?

[What is the big deal about Linux, anyway?](BIDD.pdf)

### Sharing

Do you want to do your own class? You are allowed and encouraged to use/study/copy/modify this material as long as you give credit to the author(s), and you allow your students/licensees to do the same. All SFS materials, including the Linux Camp documents, are CC BY SA. See "CC BY SA 3.0" at www.CreativeCommons.org for details.

Before delivering your own class, you should have your Linux+, LPIC-1, LFCS, or RHCSA certification or the knowledge equivalent. You should study and practice the materials ahead of your students. The more you do all those, the more successful your class will be.

We want your patches! If you found a way to make the material better, let us know so both our classes get better!

### Goals

Linux Camp was designed for two purposes, to make junior Linux sysadmins stronger with a battery of hands-on labs that are highly similar to scenarios that might occur on a real network, and by focusing those labs in the domain of a certification, to increase the chance that a given sysadmin will become certified, *without* rote memorization.

Linux Camp targets the practical exam for Linux Foundation Certified Sysadmin (LFCS). Originally, Linux Camp targeted LPI Certification Level 1 (LPIC-1).

The "Big Three" of Linux Camp are:
 - real - Labs should be about solving problems that actually happen, not contrivances.
 - hard - Labs should be satisfyingly hard, but not so hard that learners rage-quit.
 - fun  - Lab stories should be fun to read / listen to, should contain needed information, and should provide chances for students to work together.

We want to help you become creative, helpful sysadmins first, and to help you prepare, through practical experience, for the LFCS exam, second.
