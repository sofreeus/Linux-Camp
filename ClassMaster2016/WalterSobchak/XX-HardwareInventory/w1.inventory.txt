===> BASIC HARD DRIVE INFO <===
Model: ATA VBOX HARDDISK (scsi)
Disk /dev/sda: 17.2GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  419MB   418MB   primary  ext4         boot, type=83
 2      419MB   17.2GB  16.8GB  primary               lvm, type=8e



===> DISPLAYING MOUNTED PARTITIONS (df -hT) <===
Filesystem              Type      Size  Used Avail Use% Mounted on
/dev/mapper/system-root ext4      5.5G  3.8G  1.4G  74% /
devtmpfs                devtmpfs  485M   16K  485M   1% /dev
tmpfs                   tmpfs     498M     0  498M   0% /dev/shm
tmpfs                   tmpfs     498M  8.1M  490M   2% /run
tmpfs                   tmpfs     498M     0  498M   0% /sys/fs/cgroup
tmpfs                   tmpfs     498M  8.1M  490M   2% /var/lock
tmpfs                   tmpfs     498M  8.1M  490M   2% /var/run
/dev/sda1               ext4      379M   87M  269M  25% /boot
/dev/mapper/system-home ext4      8.3G   25M  7.8G   1% /home
rackmaster:/srv/commons nfs4       64G   39G   26G  60% /net/rackmaster/srv/commons


===> DISPLAYING MOUNTED PARTITIONS (df -hT) <===
/dev/disk:
total 0
drwxr-xr-x 2 root root 360 Aug 21 09:22 by-id
drwxr-xr-x 2 root root 200 Aug 21 09:22 by-path
drwxr-xr-x 2 root root 120 Aug 21 09:21 by-uuid

/dev/disk/by-id:
total 0
lrwxrwxrwx 1 root root  9 Aug 21 09:22 ata-VBOX_CD-ROM_VB2-01700376 -> ../../sr0
lrwxrwxrwx 1 root root  9 Aug 21 16:07 ata-VBOX_HARDDISK_VB7c1923fe-c0c0cd2a -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:22 ata-VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 ata-VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part2 -> ../../sda2
lrwxrwxrwx 1 root root 10 Aug 21 09:22 dm-name-system-home -> ../../dm-0
lrwxrwxrwx 1 root root 10 Aug 21 11:03 dm-name-system-root -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 dm-name-system-swap -> ../../dm-2
lrwxrwxrwx 1 root root 10 Aug 21 09:22 dm-uuid-LVM-pIVi06yd5bxBY042Kpe0aiXaYsSwG0daAeOsfT9JbBbZB413eFhN3fo4rrXtMo62 -> ../../dm-2
lrwxrwxrwx 1 root root 10 Aug 21 11:03 dm-uuid-LVM-pIVi06yd5bxBY042Kpe0aiXaYsSwG0daR6kIYua0OMhY0J5Hw8WAm6gG1Fuj9EB0 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 dm-uuid-LVM-pIVi06yd5bxBY042Kpe0aiXaYsSwG0dashnPDpS2kiRJVIc9zbTG0dfOgdTe3Zte -> ../../dm-0
lrwxrwxrwx 1 root root  9 Aug 21 16:07 scsi-1ATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:22 scsi-1ATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 scsi-1ATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part2 -> ../../sda2
lrwxrwxrwx 1 root root  9 Aug 21 16:07 scsi-SATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:22 scsi-SATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 scsi-SATA_VBOX_HARDDISK_VB7c1923fe-c0c0cd2a-part2 -> ../../sda2

/dev/disk/by-path:
total 0
lrwxrwxrwx 1 root root  9 Aug 21 09:22 pci-0000:00:01.1-ata-2.0 -> ../../sr0
lrwxrwxrwx 1 root root  9 Aug 21 09:22 pci-0000:00:01.1-scsi-2:0:0:0 -> ../../sr0
lrwxrwxrwx 1 root root  9 Aug 21 16:07 pci-0000:00:0d.0-ata-1.0 -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:22 pci-0000:00:0d.0-ata-1.0-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 pci-0000:00:0d.0-ata-1.0-part2 -> ../../sda2
lrwxrwxrwx 1 root root  9 Aug 21 16:07 pci-0000:00:0d.0-scsi-0:0:0:0 -> ../../sda
lrwxrwxrwx 1 root root 10 Aug 21 09:22 pci-0000:00:0d.0-scsi-0:0:0:0-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 pci-0000:00:0d.0-scsi-0:0:0:0-part2 -> ../../sda2

/dev/disk/by-uuid:
total 0
lrwxrwxrwx 1 root root 10 Aug 21 11:03 1968cd61-c564-4554-8cd1-78dace79fb29 -> ../../dm-1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 318a0974-e99d-4a6b-a964-1e8805e6d850 -> ../../sda1
lrwxrwxrwx 1 root root 10 Aug 21 09:22 40d2786b-fe9c-44c7-8ff1-eae518995d13 -> ../../dm-2
lrwxrwxrwx 1 root root 10 Aug 21 09:22 a929a42f-83a4-4145-b076-913041162ddf -> ../../dm-0


===> LIST BLOCK PARTITIONS (lsblk) <===
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   16G  0 disk 
├─sda1            8:1    0  399M  0 part /boot
└─sda2            8:2    0 15.6G  0 part 
  ├─system-home 253:0    0  8.5G  0 lvm  /home
  ├─system-root 253:1    0  5.7G  0 lvm  /
  └─system-swap 253:2    0  1.5G  0 lvm  [SWAP]
sr0              11:0    1 1024M  0 rom  


===> PARTED (parted -l) <===


===> DISPLAY ATTRIBUTES OF VOLUME GROUPS (vgdisplay) <===


NAME="sda" FSTYPE="" LABEL="" UUID="" MOUNTPOINT=""
NAME="sda1" FSTYPE="" LABEL="" UUID="" MOUNTPOINT="/boot"
NAME="sda2" FSTYPE="" LABEL="" UUID="" MOUNTPOINT=""
NAME="system-home" FSTYPE="" LABEL="" UUID="" MOUNTPOINT="/home"
NAME="system-root" FSTYPE="" LABEL="" UUID="" MOUNTPOINT="/"
NAME="system-swap" FSTYPE="" LABEL="" UUID="" MOUNTPOINT="[SWAP]"
NAME="sr0" FSTYPE="" LABEL="" UUID="" MOUNTPOINT=""
