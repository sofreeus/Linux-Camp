#!/bin/bash -eu

# make sure that they all have accounts on all the machines under your control and that their UID's and GID's are consistent across machines
# Create user-accounts for yourselves, me, Kim, Sam, Charlie, Dani, Eve, Fred, and Gert.

declare -i STARTING_UID
STARTING_UID=11000

for user in alice bob charlie dani eve fred gert jan kim sam
do 
	sudo useradd -m -u $STARTING_UID $USER
  let STARTING_UID=$STARTING_UID+1
	echo -e "Fr33d0mus\nFr33d0mus" | sudo passwd
done

# Create two groups: 'contractors' should have Dani, Eve, and Fred, and 'linux_admins' should have you two, me, and Eve.

sudo groupadd contractors
for user in dani eve fred
do 
	sudo usermod -aG contractors $user
done
#
#Make sure all the contractors' accounts expire in six months.

