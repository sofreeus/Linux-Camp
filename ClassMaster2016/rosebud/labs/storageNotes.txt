# Lab: Increase disk space

## centOS

### commands
lvs
vgs
pvs
lsblk
vgextend centos /dev/sdb1
vgs	
	
### create logical volume
sudo lvcreate centos -l 50%FREE -n lv_nas 

df -ht
sudo mkfs.xfs /dev/mapper/centos-lv_nas 
(all logical volumes appear under dev/mapper)
lvdisplay will print lv path
sudo mount /dev/mapper/centos-lv_nas /mnt
sudo umount /mnt
sudo mkdir /srv/nas  #create mount point
sudo chattr +i /srv/nas  #make immutable so is not a container

cat /proc/mounts
use output to populate fstab


# lab part II
#> load nfs kernel server bundle
sudo yum groups list 
yum groups install file-server  <<this one
yum groups install file-print-server-environment  (too much)

samba is a file sharing system that is unix and windows friendly

exportfs <machine specifier> <>  loads in memory; lost after reboot
make config:
vim /etc/exports
copy from lab notes: https://gitlab.com/sofreeus/linux_camp/blob/master/class/Lab-NAS-II.md

all-squash : do all & not just root. everyone treated as guest. same user may not have same uid across systems, making authorized access problematic.
no-subtree check: 
insecure: allow clients coming through NAT, otherwise nfs will cut off
anonuid are what to squash to, i.e., guest

systemctl restart nfs
systemctl status nfs
systemctl list-unit-files nfs*
showmount -e s1
showmount 
cat /etc/exports | sudo tee /etc/exports.d/nas.exports  (name anything.exports)
sudo mv /etc/exports.d/nas.export /etc/exports.d/foobar.exports
systemctl restart nfs-server
showmount -e

sudo /sbin/mount.nfs4 s1:/srv/nas /mnt
touch /mnt/afile
cd /srv/nas
ls -ld .
sudo chmod o+w .
touch /mnt/afile

To export file system:
* load pkg

* write configs
* start service
* test service
* enable service


Don't always have autofs for auto mounting
ll /net/s1.rosebud.macguffins.test/srv
sudo ln -s /net/s1.rose....  /rosebud
edit fstab
s1.rosebud.macguffins.test:/srv/nas /rosebud nfs4 soft,intr 0 0

make an interruptable soft mount 
soft fails faster when unavailable
interruptable can kill even when trying
sudo mkdir /rosebud
sudo chattr +i /rosebud
sudo lsattr /rosebud -d
now that is immutable, is a mount point instead of a container
sudo mount /rosebud
or mount -av to mount all that would be mounted on boot, verbose