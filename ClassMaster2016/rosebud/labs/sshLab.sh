# generate ssh key and distribute to the two camp servers

# Generate ssh key pair.
#   Use the default location for the key files, .ssh/id_rsa
#   Will be prompted for a pass phrase
ssh-keygen

# Transfer ssh key to s1 & s2
#   First ssh will ask you if you are sure and prompt for pass-phrase
ssh-copy-id s1
ssh-copy-id s2
