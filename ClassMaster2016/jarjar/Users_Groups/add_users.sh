#!/bin/bash -eu
# Author Justin Lang <justinslang88@gmail.com>
# Script to create users and put them in groups for Linux Camp 2016

users="alice jan bob kim sam charlie dani eve fred gert"
groups="contractors linux_admins"
contractors="dani eve fred"
admins="alice bob jan eve"

uid=1200

for user in $users
do
	sudo useradd -m -o -u $uid -s /bin/bash $user
	uid=$((uid+1))
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "I dun fucked up making $user"
	fi
done

for user in $users
do
	echo -e "Derp8!\nDerp8!" | sudo passwd $user
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "Damnit the password change didn't work for $user"
	fi
done

for group in $groups
do
	sudo groupadd $group
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "I dun fucked up making $group"
	fi
done

for fake_employee in $contractors
do
	sudo usermod -e 2017-02-21 -aG contractors $fake_employee
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "I couldn't put $fake_employee in the contractors group"
	fi
done

for sudoers in $admins
do
	sudo usermod -aG linux_admins $sudoers
	rc=$?
	if [ $rc -ne 0 ]; then
		echo "Couldn't make $sudoers global sudoers"
	fi
done
