 
Physical Disk: sda
Partition Table: dos
 
---File System Type---
/dev/sda1 : Linux
/dev/sda2 : LVM
 
---Partition Size---
sda1 : 500M
sda2 : 7.5G
 
---Percent Used---
/dev/sda1 : 39%
 
---Partition UUID---
  
