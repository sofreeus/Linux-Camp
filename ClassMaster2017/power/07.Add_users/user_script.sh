#!/bin/bash -eu

# Create user-accounts for alice, bob, jan, Kim, Sam, Charlie, Dani, Eve, Fred, and Gert.
users="alice bob jan kim sam charlie dani eve fred gert"

# Create two groups:
#   'contractors' should have Dani, Eve, and Fred
#   'linux_admins' should have you two, me, and Eve.
groups="contractors linux_admins"
contractors="dani eve fred"
linux_admins="alice bob jan eve"

# Make all the contractors' accounts expire in six months (one day for demo).
tomorrow=$(date -d '+1 day' --iso)

case "$1" in
	"up")
		echo "Creating user accounts..."
		userid=10000
		groupid=20000
		for user in $users
			do useradd -m -d /home/$user -k /etc/skel -u $userid -U $user
			userid=$(( $userid + 1 ))
		done
		for group in $groups
			do groupadd -g $groupid $group
			groupid=$(( $groupid + 1 ))
		done
		for contractor in $contractors
			do usermod -aG contractors -e $tomorrow $contractor
		done
		for admin in $linux_admins
			do usermod -aG linux_admins $admin
		done
		for user in $users
			do id $user
		done
		echo "Contractor account expirations:"
		for contractor in $contractors
			do echo -n "$contractor "
			chage --list $contractor | grep "Account expires"
		done
		;;
	"down")
		echo "Destroying user accounts..."
		for user in $users
			do echo "Removing user: $user..."
			# Handling errors here because it'll try to remove files that don't exist
			userdel -r $user &>/dev/null || true
		done
		for group in $groups
			do echo "Removing group: $group..."
			groupdel $group &>/dev/null
		done
		;;
	*)
		echo "Please run this script with an 'up' or a 'down' argument"
		;;
esac
