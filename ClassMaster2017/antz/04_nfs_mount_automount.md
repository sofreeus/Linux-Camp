# connect to nfs mount - answer key - mount and automount

 *a client or clients mount a volume on a server (share), to save large files*

***

## 0) ensure nfs services are installed and active

* check if any nfs services already installed: `systemctl list-unit-files | grep nfs`
  * you should only see a few old "masked" services at first


* for centos/redhat/fedora (UBUNTU USERS SKIP TO NEXT):

  * `sudo yum provides "nfs*" | grep nfs` to figure out what to install

  * `sudo yum install nfs-utils` for both server and client


* for UBUNTU:
  * `sudo apt search nfs | grep nfs` to figure out what to install

  * for server `sudo apt install nfs-kernel-server`
  and for client `sudo apt install nfs-common`

* now check for installed nfs services again: `systemctl list-unit-files | grep nfs`
* you should see more nfs services available than before

* check status: `systemctl status nfs`... **click tab 2-3 times** after typing nfs, 'nfs-' service options will appear. Make sure these are started.

  *   `systemctl status nfs-server`... `systemctl start nfs-server`... `systemctl enable nfs-server`

  * etc...

***

## 1) manual mount nfs
* **on the server (s1 or s2)**, create the share folder, change permissions and owner of shared folder:
  * `mkdir /var/nfs`

  * `chmod -R 755 /var/nfs/` --- OR ---   `chmod -R u+rwx,go-w,go+rx /var/nfs/`

  for centos/redhat/fedora (UBUNTU USERS SKIP TO NEXT):

  * `chown nobody:nobody /var/nfs/`

  for UBUNTU:

  * `chown nobody:nogroup /var/nfs/`


* create export file *(instructs the server which clients get access to shares and the local path to each share and specifies restrictions with options)*

 * `vim /etc/exports`

contents of /etc/exports:
```bash

 /var/nfs *(rw,all_squash,insecure,no_subtree_check,anonuid=99,anongid=99)
 /home *(rw,no_root_squash)
```

* notes on /etc/exports:
 * the **'*'** means it will accept any clients. We could specify IP addresses or other client names on the network instead of '*', but in this case we will allow all.

 * **rw** Allows read and write access to the share,
**ro** Only allows read access, writing is blocked

 * **all_squash**
Forces all connecting users to "nobody" account and permissions, **no_root_squash** Allows root account on client to access export share on server as the root account

 * **anonuid=[insert uid]** Forces all anonymous connections to predefined UID on server, **anongid=[insert gid]** Forces all anonymous connections to predefined GID on server


* restart nfs service
 * `systemctl restart nfs-server`
* from the client, check that the server mounts specified in /etc/exports are being shared:
  * `showmount -e 192.168.0.200`
    ```bash
    Export list for 192.168.0.200:
    /var/nfs *
    /home    *

    ```

    (sub in your actual server IP address or name)

for centos/redhat/fedora(for UBUNTU SKIP TO NEXT):
* **on the server**, add firewall exceptions OR just take down the firewall

  to make firewall exceptions:

  * `firewall-cmd --permanent --zone=public --add-service=nfs`
  * `firewall-cmd --permanent --zone=systempublic --add-service=mountd`
  * `firewall-cmd --permanent --zone=public --add-service=rpc-bind`
  * `firewall-cmd --reload`
  * `systemctl restart nfs-server`

  to take down the firewall:

  * `systemctl stop firewalld`


for UBUNTU:
* **on the server**, add firewall exceptions OR just take down the firewall

  to make firewall exceptions:
  * `ufw allow nfs`
  * `ufw allow sunrpc`

  * fix rpc to one port:
    `vim /etc/defaults/nfs-kernel-server`
    * change the line for RPCMOUNTDOPTS to:
    ```bash
     RPCMOUNTDOPTS=”-p 4001 -g”
     ```

  * `ufw allow 4001`

  * `ufw reload`

  * `systemctl restart nfs-server`

  to take down the firewall:

  * `systemctl stop ufw`

    or

  * `ufw disable`


* **on the client(w1)**, create directories to correspond to server-side shares
  * `mkdir -p /mnt/nfs/var/nfs/`
  * `mkdir -p /mnt/nfs/home/`


* mount the server share from the client side

  * `mount -t nfs 192.168.0.200:/var/nfs/ /mnt/nfs/var/nfs/`
  (sub in your actual server IP address or name)


* create a file on the client side path to test
  * `touch /mnt/nfs/var/nfs/test_nfs`
* confirm the file exists on the server side. Access the server through ssh from client and then go to shared directory
  * `ssh user@192.168.0.200` (replace user with the actual user and the IP address with the actual server IP address)
  * `ls /var/nfs/` (you should see the file test_nfs here!)


* **make mount permanent** by adding to **fstab** of client
  * `vim /etc/fstab`

contents of /etc/fstab:
```bash

192.168.0.200:/var/nfs    /mnt/nfs/var/nfs/   nfs defaults 0 0
192.168.0.200:/home    /mnt/nfs/home   nfs defaults 0 0
```
 (sub in your actual server IP address or name)

* reboot the client and check that the mount points to server and client are still connected and file is present on both sides.

* notes on fstab:
  * server side path for mount.... then client side path for mount... then file system type (nfs in this case), then options (these are similar to /etc/export options on the server side, were going with defaults for this example), then two zeros.
  * what are the two zeros?
    * first zero means "don't back this up". Switch it to "1" and the file system will be backed up.
    * second zero determines if fsck will be run on it (leave at zero for nfs, it does not apply). fsck checks and repairs damaged file systems. 0 means don't do fsck, 1 means do fsck first, 2 means do fsck after all "1"s have been checked. fsck happens on boot. Typically root partition is set to 1. All other local are set to 2. Remote stuff is set to 0.


* view/verify all active mounts: `mount -l`
* unmount: `umount /mnt/nfs/var/nfs/`
* view/verify that this mount is no longer there: `mount -l`

---

## 2) automount nfs
* on the client, remove or comment out the /etc/fstab entries you just created, automount does not use /etc/fstab! autofs uses /etc/auto.master

new contents of /etc/fstab:
```bash

#192.168.0.200:/var/nfs    /mnt/nfs/var/nfs/   nfs defaults 0 0
#192.168.0.200:/home    /mnt/nfs/home   nfs defaults 0 0
```
 * for centos/redhat/fedora (UBUNTU USERS SEE BELOW):

   * `sudo yum provides "auto*" | less` to figure out what to install

   * `sudo yum install autofs` on client

* for UBUNTU:
     * `sudo apt search auto | less` to figure out what to install

     * `sudo apt install autofs` on client

* several new files appear in etc:  /etc/auto*
     * edit auto.master file: `vim /etc/auto.master`
      * uncomment the '/net...' line, the final should look like this when done:

        `cat /etc/auto.master`
 ```bash
#
# Sample auto.master file
# This is a 'master' automounter map and it has the following format:
# mount-point [map-type[,format]:]map [options]
# For details of the format look at auto.master(5).
#
#/misc	/etc/auto.misc
#
# NOTE: mounts done from a hosts map will be mounted with the
#	"nosuid" and "nodev" options unless the "suid" and "dev"
#	options are explicitly given.
#
/net	-hosts
#
# Include /etc/auto.master.d/*.autofs
# The included files must conform to the format of this file.
#
+dir:/etc/auto.master.d
#
# Include central master map if it can be found using
# nsswitch sources.
#
# Note that if there are entries for /net or /misc (as
# above) in the included master map any keys that are the
# same will not be seen as the first read key seen takes
# precedence.
#
+auto.master
```

* ensure autofs is active and running: `systemctl status autofs`...and if needed... `systemctl start autofs`

* showmount once more to be sure nfs exports are available: `showmount -e 192.168.0.200`

* now you can seamlessly and automatically mount your nfs server from anywhere on your client as long as the path you specify begins with "/net" followed by your server IP address or name:
  * `ls /net/192.168.0.200/var...` (sub in your actual server ip address)


* result should be a listing of the **nfs** directory on the nfs server
* go into **nfs** directory: `ls /net/192.168.0.200/var/nfs` (sub in your actual server ip address)
* you should see the **test_nfs** file we created in the manual mount exercise

* now create a symlink from nfs server to client home:
  * `cd ~`
  * `ln -s /net/192.168.0.200/var/nfs`

    the **nfs** directory from the nfs server should now appear in your client home directory along side the other directories.

  * display the long listing of the home directory. the symlink to the **nfs** directory should show:
  `ls -l nfs`

  ```

  lrwxrwxrwx  1 admin admin   32 Nov 10 19:28 nfs -> /net/192.168.0.200/var/nfs//
  ```

* another way to check that your mount worked: `df -h`
