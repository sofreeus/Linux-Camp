## setting up ssh keys
connecting the workstation(w1) to the servers (s1 and s2) through ssh using rsa certs so passwords not needed for 'sshing' from client to server (except password for key itself)

---

**0)** add hostname of server to /etc/hosts file of workstation(a.k.a. client)

* on the client type `vim /etc/hosts`
* add this line:

```
192.168.0.200 s1.virtual.com    s1 # s1 is an alternate short name for the server

```
  (sub in your actual server IP or name)

---

* if you have a second server, add that one in as well:

```

192.168.0.200 s1.centos.com    s1
192.168.0.200 s2.ubuntu.com    s2

```

**1)** generate ssh keys on client
* on the workstation (client), generate an ssh key:
  * `ssh-keygen`

```

Generating public/private rsa key pair.
Enter file in which to save the key (/home/anthony/.ssh/id_rsa):
```

* click **enter** to use the default path and file name

```

Enter passphrase (empty for no passphrase):
```
* enter a passphrase

* you should now have a private key file: ~/.ssh/id_rsa

* and also a public key file: ~/.ssh/id_rsa.pub

on your workstation.

* copy the id_rsa.pub
file to the server:

  * `ssh-copy-id 192.168.0.200`

    (sub in your actual server IP or name)

* the server should now have the contents of the client's id_rsa.pub key in it's authorized_keys file (~/.ssh/authorized_keys). With this, the server won;t require a password form the client when client ssh connects to the server. It will compare its own copy of the id_rsa.pub key to the clients and get a match.

* now connect to server from cient:
  * `ssh admin@192.168.0.200`
  * the first time you will have to jump through some minor hoops to get into the server. once in, immediately `exit` and ssh in again. The second time you should be able to get right in (except for entering the password for the id_rsa.pub key itself)
