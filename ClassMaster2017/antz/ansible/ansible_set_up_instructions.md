# download and set up ansible

download ansible

set up hosts file

review "create_users.yml"

run "create_users.yml"
***

**(0)** on your workstation, download ansible:

* for centos/rh/fedora (SEE BELOW FOR UBUNTU)
  * `yum install ansible`

* for UBUNTU
  * `apt install ansible`

NOTE - once download is complete, ansible will pur some new folders on your workstation including, most importantly for us: /etc/ansible/

* open the ansible hosts file and enter your server names or IP addresses:
  * `vim /etc/ansible/hosts`

* enter group names and server names into the file:


```

[webservers1]
s1

[webservers2]
s2
```
**NOTE** - I used "s1" and "s2" because that's the short name I used in my /etc/hosts file in exercise 03

ansible's hosts file will use /etc/hosts file to determine the ip address


***

* next we need to **turn our user builder on our servers into a password-less sudoer** (we need to be able to do 'root' things on the servers without entering a password, otherwise Ansible won't work!)


* ssh into one of your servers:

    `ssh builder@s1`

    --or--

    `ssh builder@[server.ip.addr.ess]`

* open visudo file: `sudo visudo`

* enter this line at the bottom of the file:

```

builder ALL=(ALL)    NOPASSWD: ALL
```

* save and exit the file with shift+zz

**NOTE** - the visudo file writes your changes to the actual sudoers file in a safe way

its the sudoers file that actually controls which users have permission to do what

view the real sudoers file and see that what you wrote in visudo is now in the sudoers file: `cat /etc/sudoers`

* now exit the first server: `exit`

* ... and go do the same ting to your second server





***

ansible command we will use today:

`ansible-playbook make_webserver.yml`

another file that will be called by the ansible playbook: index.html.j2 (this is a jinja template)
