#!/bin/bash -eux

DEST=/shared/backups
FILE=etc-$(date --iso)-$(hostname).tar

tar cvf $DEST/$FILE -C / etc

bzip2 $DEST/$FILE

#find $DEST -mtime +7 -exec rm {} \;
