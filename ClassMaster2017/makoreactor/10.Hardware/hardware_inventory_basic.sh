#!/bin/bash -eu

cpu_info=$( lscpu )
cpu_model=$( printf "$cpu_info" | grep "^Model name:" )
cpu_num=$( printf "$cpu_info" | grep "^CPU(s):" )

echo -n 'Please run `free` in another window and input the total memory here: '
read mem_total

echo "===
Copy and paste the following into an e-mail to StrongJan/CookieMonster:
---

Processor Information:
$cpu_model
$cpu_num

Memory total: $mem_total kB
"
