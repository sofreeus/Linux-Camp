Challenge: cdir script
Notes: I don't know if we'll get this far, but if we do, let's write a script that uses locate to do a trick.
If arg1 is a directory that only exists in one place on the system, cdir changes to that directory directly. If not, it returns a list of matching directory-paths. Gotcha: What if arg1 is a unique file?