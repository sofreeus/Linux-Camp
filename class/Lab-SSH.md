Lab: Setting up ssh keys
===

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please generate password-protected ssh keys for yourselves, and authorize them on all the servers, so that you can sign into either server without entering a password (other than the one you enter to unlock your keyring or your private key).

> Don't forget that .ssh/ and .ssh/authorized_keys need to be restrictively permissioned. `chmod go-rwx --recursive ~/.ssh` does it, I think.

> --Jan

---

Notes:

Install the public key on one server manually, and on the other using `ssh-copy-id`.
