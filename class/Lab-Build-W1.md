Lab: Build a Workstation (W1)
=============================

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> Please build a workstation to do your work from. Make sure to name the first user 'builder' or something else generic. Security wants the build users removed, but don't do that until after you create the user accounts.

> Try to setup something for screen-sharing over the network so you can pair over distance.

> --Kim

---

Notes
-----

Do not worry about creating user accounts as part of this lab.  Ergo, do not worry about removing the build user yet.

You may want to install a vnc and/or xRDP servers so that you can easily get a remote graphical shell on this machine.

Suggested values:
- Distro: [openSUSE](https://www.opensuse.org/)
- VM name: W1
- hostname: W1.(student-project-name).test
- hostname example: W1.alicorn.test
- Memory: 4GB
- Storage: 32GB with LVM
- network: "Bridged" or "NAT Network" is good. "NAT" is bad. Guests (W1, S1, S2) need to see each other. "Bridged" and "NAT Network" allow that. Use "Bridged" if your host will not change networks, and you want to be able to reach the guests from machines other than the host. If your host will change networks, use "NAT Network". Consider a "Bridged" secondary adapter, if you'll be changing networks with the host and you want the guests visible to other machines.

Guest Additions:
- Desktop automatically resizes to the window
- Clipboard works bidirectionally

You might need to install the Guest Additions. If so, from Devices menu, insert Guest Additions CD.

Try a shared vnc/xRDP/byobu/tmux/screen session with your partner/s

After install is complete, shut the machine down and move it into a "Linux Camp" group. If you have a lot of machines in VirtualBox, enter the group by clicking the right-pointing triangle, to reduce visual clutter.

Hints
---

Use `hostnamectl` to set hostnames.

Remmina is an awesome and actively developed multi-protocol/multi-session terminal client. Vinagre is headed for the graveyard.

xRDP:
- install package `xrdp`
- open firewall to allow `3389/tcp`
- connect with native RDP client (i.e., you're running Windows) or `xfreerdp` or `remmina` in Linux or (?) in Mac

vnc:
- If you chose GNOME, try GNOME screen sharing: settings, sharing, system, sharing, 
- Try `vncviewer -listen` and `x11vnc -connect (listener)` if the viewer is reachable, but the server is not.
- Try x11vnc

screen:
- this user: `screen -S somename`
- that user: `screen -x`

Further reading:
- https://people.gnome.org/~markmc/remote-desktop.html
