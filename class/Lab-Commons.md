Lab: Accessing the Commons
==========================

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice/Bob,

> We have a 'commons' share where employees can easily post large or fast-changing files (rather than sending them in email). Please make sure that all machines have easy access to that.

> I want it to appear at /commons and every file of it should be writable from every machine for every user.

> Thanks for taking care of this,

> --Jan

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Yo A & B,

> Use autofs. It rocks. Automatically mounts needed things, and unmounts idle things. Don't hard-mount NFS over the WAN or I'll eat a kitten.

> This should be pretty easy and fast, I hope. Install autofs, enable the "/net  -hosts" line in /etc/autofs.master, create a symlink to /net/files.sofree.us/srv/commons in your root directory...

> Remember to *enable* autofs or you'll wish you had, right around the time the first employee gets in, the morning after maintenance.

> --K

---

### Process Notes

For machine in machines, do
  - install autofs
  - enable /net
  - start and test autofs
  - enable autofs
  - symlink /commons

For the host machine, the above may not be desired or even possible.

In that case, try SMB and NFS in the GUI of whatever you have. These connection strings might help.

```
# From the Windows GUI
\\files.sofree.us\commons
# From the GUI, almost any file browser
nfs://files.sofree.us:/srv/commons
smb://files.sofree.us/commons
# From the CLI, using temporary static NFS mount
sudo mount.nfs4 files.sofree.us:/srv/commons /mnt/
sudo mount.smb3 //files.sofree.us/commons /mnt/ -o guest
# Remember to unmount when done
sudo umount /mnt/
# Consider adding a 'noauto' to /etc/fstab
sudo mkdir /mnt/{nfs,smb}
sudo chattr +i /mnt/{nfs,smb}
echo "files.sofree.us:/srv/commons /mnt/nfs nfs4 noauto       0 0" | sudo tee -a /etc/fstab
echo "//files.sofree.us/commons    /mnt/smb smb3 guest,noauto 0 0" | sudo tee -a /etc/fstab
mount -v /mnt/nfs
mount -v /mnt/smb
umount /mnt/*
```

### Hints of commands and configs

- yum
- zypper
- apt

- systemctl
- service

- /etc/auto.master

- Packages:
  - autofs
  - nfs-utils (?)