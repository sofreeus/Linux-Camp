Lab: Configuring networking settings
------------------------------------

---

> FROM: Jan <jan@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> Alice and Bob,

> I am just crazy old-fashioned and I'd really rather you put those servers on manually-configured static IP addresses, instead.

> Thanks!

> --Jan

---

> FROM: Kim <kim@macguffins.test>

> TO: Alice <alice@macguffins.test>, Bob <bob@macguffins.test>

> static ip addresses. wow. crazy's right.

> networking is easy on enterprise linux, but make sure you check the ifcfg-blah files in /etc/sysconfig/network-scripts/ and /etc/sysconfig/network
> on enterprise linux 7. use `nmtui` to get a basic setup

> on debian-ish distros, the man page for 'resolvconf' has a great example to start from, but you'll need to edit the /etc/network/interfaces file.  check the man page for 'interfaces'

> if you have anything else other than enterprise, debian, or opensuse, you're on your own

> always, always double-check /etc/hosts, and make sure that you synchronize the host file entries across all nodes each time you change an ip

> always keep the hosts entries in sync

> --Kim

---

Notes:

 * nmtui
 * /etc/network/interfaces
 * /etc/hosts
 * /etc/resolv.conf

For each of your servers, configure eth0 with:

  * Static IP address
  * subnet mask

Confirm these settings survive the change:

  * network / subnet
  * gateway / router
  * nameserver / domain

Confirm that all the machines can still see each other. You will probably need to update hostlines.

Distribution-specific tips

 * Ubuntu (Desktop): Use the GUI interface to Network Manager
 * Ubuntu (Server) and Debian: Edit `/etc/network/interfaces` or `/etc/netplan/*`
 * Use the man pages for `resolvconf` and `interfaces` or `netplan`
 * CentOS: Use `nmtui` and/or edit:
    - /etc/sysconfig/network-scripts/ifcfg-blah

Verify all host-lines in /etc/hosts files on all machines follow the form:

```
ip.address fully-qualified-name alias1 alias2 alias3
```

Ensure you're using your assigned IP addresses per [the chart that was introduced earlier](https://gitlab.com/sofreeus/linux_camp/blob/master/chart-ip-addresses.md).

For example:

```
185.78.197.255 RackMaster.MacGuffins.Test RackMaster ClassMaster

10.168.134.005 W1.alicorn.MacGuffins.Test W1
10.168.134.006 S1.alicorn.MacGuffins.Test S1
10.168.134.007 S2.alicorn.MacGuffins.Test S2
```

Confirm that each machine returns the correct FQDN to `hostname -f` and the correct short hostname to `hostname -s`

Use `ip addr show` and `ip route`, at least.
